#include <iostream>
#include <xmmintrin.h>
#include "du1simd.hpp"

using namespace std;

int main() {
  simd_vector<float, __m128> v(10);

  auto it = v.begin();

  ++it;

  auto x1 = it.lower_block();
  auto x2 = it.lower_block();
  auto y1 = it.upper_block();
  auto y2 = it.upper_block();

  cout << "lower offset: " << it.lower_offset() << endl;
  cout << "upper offset: " << it.upper_offset() << endl;

  cout << "x1: " << &*x1 << " x2: " << &*x2 << endl;
  cout << "y1: " << &*y1 << " y2: " << &*y2 << endl;

  cout << "++x2" << endl;
  ++x2;

  cout << "x2: " << &*x2 << " y1: " << &*y1 << endl;

  cout << "--y2" << endl;
  --y2;

  cout << "x1: " << &*x1 << " y2: " << &*y2 << endl;

}
