#include <pmmintrin.h>
#include <iterator>
#include <vector>
#include <cmath>

template <typename T, typename S>
class simd_vector
{
  // S, bloky
  class simd_iterator
  {
    public:
      S* current;

    public:
      simd_iterator(S* ptr): current(ptr) {}

      bool operator==(simd_iterator& other) { return current == other.current; }
      bool operator!=(simd_iterator& other) { return current != other.current; }

      simd_iterator& operator++() { current += sizeof(S); return *this; }
      simd_iterator& operator--() { current -= sizeof(S); return *this; }

      simd_iterator& operator+=(std::size_t offset) { current += offset; return *this; }
      simd_iterator& operator-=(std::size_t offset) { current -= offset; return *this; }

      S& operator*() { return (S&)*current; }
  };

  // T, itemy
  class simd_vector_iterator
  {
    public:
      T* begin;
      T* current;
      const std::size_t ratio = sizeof(S)/sizeof(T);

    public:
      simd_vector_iterator(T* ptr): begin(ptr), current(ptr) {}

      simd_vector_iterator& operator++() { current++; return *this; }
      simd_vector_iterator& operator--() { current--; return *this; }
      simd_vector_iterator& operator+=(std::size_t offset) { current += offset; return *this; }

      T& operator*() { return *current; }

      bool operator==(simd_vector_iterator& other) { return current == other.current; }
      bool operator!=(simd_vector_iterator& other) { return current != other.current; }

      simd_vector_iterator& operator+(std::size_t amount) { current += amount; return *this; }

      // S = 16, T = 4
      simd_iterator lower_block() {
        std::size_t t_offset = current - begin;
        std::size_t ratio = sizeof(S)/sizeof(T); // 4

        std::size_t s_offset = std::floor(t_offset / ratio); // 2

        S* ptr = ((S*)begin) + s_offset;
        return simd_iterator(ptr);
      }

      simd_iterator upper_block() { return ++lower_block(); }

      int lower_offset() { return (current - begin) % ratio; }
      // The order here is reversed because upper offset should be negative
      int upper_offset() { return lower_offset() - ratio; }

      void scroll_end() {}
  };

  public:
    typedef simd_vector<T, S>::simd_vector_iterator iterator;
    typedef simd_vector<T, S>::simd_iterator simd_iterator;

    explicit simd_vector(std::size_t s): _data(s) {}

    simd_vector_iterator begin() { return iterator(begin_ptr_t()); }
    simd_vector_iterator end()   { return iterator(end_ptr_t()); }

    std::size_t size() { return _data.size(); }

  private:
    T* begin_ptr_t() { return &*_data.begin(); }
    T* end_ptr_t()   { return &*_data.end(); }
    S* begin_ptr_s() { return (S*)&*_data.begin(); }

    std::vector<T> _data;
};
