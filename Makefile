CC=clang++
CFLAGS=-std=c++11 -stdlib=libc++ -Wall -Wextra -pedantic -g -msse3

run: build
	./main

build:
	$(CC) $(CFLAGS) main.cpp -o main

build_test:
	$(CC) $(CFLAGS) du1test.cpp -o du1test

test: build_test
	./du1test
